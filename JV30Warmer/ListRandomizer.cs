﻿using System;
using System.Collections.Generic;

namespace Monster.UITest.JV30Warmer
{
    internal class ListRandomizer<T>
    {
        private static readonly Random Rng = new Random();
        private readonly int _numOfRepeats;
        private List<T> _result;

        public ListRandomizer(int numOfRepeats)
        {
            _numOfRepeats = numOfRepeats;
        }

        public ListRandomizer():this(1)
        {
            
        }

        public IList<T> Randomize(IEnumerable<T> inputSequence)
        {
            _result = new List<T>();
            foreach (var inputItem in inputSequence)
            {
                for (int i = 0; i < _numOfRepeats; i++)
                {
                    _result.Add(inputItem);
                }
            }

            Shuffle();
            return _result;
        }

        private void Shuffle()
        {
            var n = _result.Count;
            while (n > 1)
            {
                n--;
                var k = Rng.Next(n + 1);
                var value = _result[k];
                _result[k] = _result[n];
                _result[n] = value;
            }
        }
    }
}
