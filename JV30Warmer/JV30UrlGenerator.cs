﻿using System;
using System.Collections.Generic;

namespace Monster.UITest.JV30Warmer
{
    internal class Jv30UrlGenerator
    {
        private readonly string[] _jv30DomainList; 
        private readonly string _durationJobStandardApply;
        private readonly string _ppcJobAwmApply;
        private readonly string _aggregatedJob;

        public Jv30UrlGenerator(string durationJobStandardApply, string ppcJobAwmApply, string aggregatedJob, string[] jv30DomainList)
        {
            _durationJobStandardApply = durationJobStandardApply;
            _ppcJobAwmApply = ppcJobAwmApply;
            _aggregatedJob = aggregatedJob;
            _jv30DomainList = jv30DomainList;
        }

        public IEnumerable<string> GenerateUrls()
        {
            var generators = new Func<IEnumerable<string>>[]{ GenerateViewUrls, GenerateApplyUrls};

            foreach (var generator in generators)
            {
                foreach (var url in generator.Invoke())
                {
                    yield return url;
                }
            }
        }

        private IEnumerable<string> GenerateViewUrls()
        {
            var jobIds = new[] {_durationJobStandardApply, _ppcJobAwmApply, _aggregatedJob};
            foreach (var domain in _jv30DomainList)
            {
                foreach (var jobId in jobIds)
                {
                    yield return $"{domain}/v2/job/view?jobid={jobId}";
                }
            }
        }

        private IEnumerable<string> GenerateApplyUrls()
        {
            var jobIds = new[] { _durationJobStandardApply, _ppcJobAwmApply};
            foreach (var domain in _jv30DomainList)
            {
                foreach (var jobId in jobIds)
                {
                    yield return $"{domain}/v2/job/apply?jobid={jobId}";
                }
            }
        }
    }
}
