﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using Microsoft.Extensions.Configuration;

namespace Monster.UITest.JV30Warmer
{
    internal class Jv30Warmer
    {
        private readonly Jv30Config config;
        private readonly HttpClient httpClient;
        public IEnumerable<string> Results { get; private set; }

        private Jv30Warmer(Jv30Config config)
        {
            this.config = config;
            httpClient = ConstructHttpClient(config);
        }

        static void Main(string[] args)
        {
            var config = GetConfiguration();
            var warmer = new Jv30Warmer(config);

            var watch = Stopwatch.StartNew();
            warmer.WarmSite();
            watch.Stop();

            foreach (var result in warmer.Results)
            {
                Console.WriteLine(result);
            }
            Console.WriteLine($"Running for {watch.ElapsedMilliseconds} ms");
            Console.WriteLine("Press any key to finish the program");
            Console.ReadLine();
        }

        private static HttpClient ConstructHttpClient(Jv30Config config)
        {
            return new HttpClient {Timeout = TimeSpan.FromSeconds(config.HttpClientTimeoutInSec)};
        }

        private static Jv30Config GetConfiguration()
        {
            var envName = Environment.GetEnvironmentVariable("JV30_ENVIRONMENT");

            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false)
                .AddJsonFile($"appsettings-{envName}.json", optional: true)
                .Build();

            return configuration.GetSection("Jv30Config").Get<Jv30Config>();
        }

        public void WarmSite()
        {
            var rndList = ConstructRandomizedUrlList();
            Console.WriteLine($"Number of Urls: {rndList.Count}");
            var warmerClient = new WarmerClient(httpClient, config.MaxTasks);
            warmerClient.WarmUrlsAsync(rndList).Wait();
            Results = warmerClient.Result;
        }

        private IList<string> ConstructRandomizedUrlList()
        {
            var jv30Generator = new Jv30UrlGenerator(
                config.DurationJobStandardApply,
                config.PpcJobAwmApply,
                config.AggregatedJob,
                config.DomainsList);
            var urlRandomList = new ListRandomizer<string>(config.NumOfRepeats);

            return urlRandomList.Randomize(jv30Generator.GenerateUrls());
        }
    }
}
