﻿namespace Monster.UITest.JV30Warmer
{
    internal class Jv30Config
    {
        public string DurationJobStandardApply { get; set; }
        public string PpcJobAwmApply { get; set; }
        public string AggregatedJob { get; set; }
        public int NumOfRepeats { get;  set; }
        public int MaxTasks { get; set; }
        public string[] DomainsList { get; set; }
        public int HttpClientTimeoutInSec { get; set; }
    }
}
