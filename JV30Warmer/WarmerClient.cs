﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Monster.UITest.JV30Warmer
{
    internal class WarmerClient
    {
        private readonly HttpClient _httpClient;
        private readonly int _maxTasks;
        private readonly ConcurrentDictionary<HttpStatusCode, int> _responseCodes = new ConcurrentDictionary<HttpStatusCode, int>();

        public IEnumerable<string> Result
        {
            get { return _responseCodes.Select(entry => $"Response: {entry.Key}\tNumber of responses: {entry.Value}"); }
        }
        public WarmerClient(HttpClient httpClient, int maxTasks)
        {
            _httpClient = httpClient;
            _maxTasks = maxTasks;
        }

        public async Task WarmUrlAsync(string url)
        {
            try
            {
                var response = await _httpClient.GetAsync(url);
                if(response.StatusCode == HttpStatusCode.NotFound)
                    _responseCodes.AddOrUpdate(response.StatusCode, 1, (key, oldvalue) => ++oldvalue);
                else
                    _responseCodes.AddOrUpdate(response.StatusCode, 1, (key, oldvalue) => ++oldvalue);
            }
            catch (TaskCanceledException)
            {
                _responseCodes.AddOrUpdate(HttpStatusCode.RequestTimeout, 1, (key, oldvalue) => ++oldvalue);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task WarmUrlsAsync(IList<string> urls)
        {
            var taskList = InitialTaskList;

            foreach (var url in urls)
            {
                var taskIndex = Task.WaitAny(taskList);
                taskList[taskIndex] = WarmUrlAsync(url);
            }

            await Task.WhenAll(taskList);
        }

        private Task[] InitialTaskList
        {
            get
            {
                var taskList = new Task[_maxTasks];
                for (var i = 0; i < taskList.Length; i++)
                {
                    taskList[i] = Task.FromResult(0);
                }
                return taskList;
            }
        }
    }
}
